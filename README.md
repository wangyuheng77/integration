# integration

manage feature & bug by issue

## 新建 Issue 相关细节
- 模板：从以下 2 者中进行选择 
    - feature 
    - bug 
- Assignee：开发负责人 
- Label： 
    - 优先级 
        - P-1：全线 block 工作，直接在群里汇报，不需要走 gitlab，例如： 
            - 网站无法使用 
        - P0：block 个人工作 
        - P1：暂时不 block 工作，但是周尺度需要解决 
        - P2：暂时不 block 工作，周尺度外需要解决（配合 DDL-调整优先级） 
    - label(可选) 
        - bug 
        - feature 
    - 里程碑
        - 需求提出月份
 
# 验收-After 工程已完成测试
 
- 工程会在完成 BUG Feature 后指定相关人员做验收，默认谁提 feature BUG 谁做验收（可能会存在特殊的指定）
- 开发完成后会放入verify看板，验收通过后由author关闭issue